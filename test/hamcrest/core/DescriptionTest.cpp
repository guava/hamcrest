#include <hamcrest/base/Description.h>
#include <hamcrest/core/def/IsDef.h>
#include <magellan/magellan.hpp>

USING_HAMCREST_NS

FIXTURE(DescriptionTest)
{
    Description desc;

    DescriptionTest& given_description()
    {
        return *this;
    }

    template <typename T>
    DescriptionTest& when_append_value(const T& val)
    {
        desc.appendValue(val);
        return *this;
    }

    void then_result_is(const std::string& result)
    {
        ASSERT_THAT(desc.to_s(), is(result));
    }

    TEST("false")
    {
        given_description()
            .when_append_value(false)
            .then_result_is("<(bool)false>");
    }

    TEST("true")
    {
        given_description()
            .when_append_value(true)
            .then_result_is("<(bool)true>");
    }

    TEST("null integer")
    {
        given_description()
            .when_append_value((void*)0)
            .then_result_is("<(void*)nil>");
    }

    TEST("NULL")
    {
        given_description()
            .when_append_value((void*)NULL)
            .then_result_is("<(void*)nil>");
    }

    TEST("nullptr")
    {
        given_description()
            .when_append_value(nullptr)
            .then_result_is("<(std::nullptr_t)nullptr>");
    }

    TEST("char")
    {
        given_description()
            .when_append_value((char)2)
            .then_result_is("<(char)0x2/2>");
    }

    TEST("signed char")
    {
        given_description()
            .when_append_value((signed char)2)
            .then_result_is("<(signed char)0x2/2>");
    }

    TEST("unsigned char")
    {
        given_description()
            .when_append_value((unsigned char)2)
            .then_result_is("<(unsigned char)0x2/2>");
    }

    TEST("short")
    {
        given_description()
            .when_append_value((short)2)
            .then_result_is("<(short)0x2/2>");
    }

    TEST("signed short")
    {
        given_description()
            .when_append_value((signed short)2)
            .then_result_is("<(short)0x2/2>");
    }

    TEST("unsigned short")
    {
        given_description()
            .when_append_value((unsigned short)2)
            .then_result_is("<(unsigned short)0x2/2>");
    }

    TEST("int")
    {
        given_description()
            .when_append_value(2)
            .then_result_is("<(int)0x2/2>");
    }

    TEST("unsigned int")
    {
        given_description()
            .when_append_value(2U)
            .then_result_is("<(unsigned int)0x2/2>");
    }

    TEST("long")
    {
        given_description()
            .when_append_value(2L)
            .then_result_is("<(long)0x2/2>");
    }

    TEST("unsigned long")
    {
        given_description()
            .when_append_value(2UL)
            .then_result_is("<(unsigned long)0x2/2>");
    }

    TEST("c style string")
    {
        given_description()
            .when_append_value("hello, world")
            .then_result_is("<(char [13])\"hello, world\">");
    }

    TEST("std::string")
    {
        given_description()
            .when_append_value(std::string("hello, world"))
            .then_result_is("<(std::string)\"hello, world\">");
    }

    TEST("append text")
    {
        desc.appendText("hello")
            .appendText(", ")
            .appendText("world");

        ASSERT_THAT(desc.to_s(), is(std::string("hello, world")));
    }

    TEST("append value list")
    {
        desc.appendValueList("{", ", ", "}", std::vector<int>{1, 2});

        ASSERT_THAT(desc.to_s(), is(std::string("{<(int)0x1/1>, <(int)0x2/2>}")));
    }
};
